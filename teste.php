<?php
use Psr\Http\Message\ServerRequestInterface;
use React\EventLoop\Factory;
use React\Http\Response;
use React\Http\Server;
require __DIR__ . '/vendor/autoload.php';
$loop = Factory::create();
$server = new Server(function (ServerRequestInterface $request) {

    return new Response(
        200,
        [
            'Content-Type' => 'application/json'
        ],
        teste()
    );
});
//$socket = new \React\Socket\Server(isset($argv[1]) ? $argv[1] : '10.1.1.103:8090', $loop);
$socket = new \React\Socket\Server(isset($argv[1]) ? $argv[1] : '127.0.0.1:8090', $loop);
$server->listen($socket);
echo 'Listening on ' . str_replace('tcp:', 'http:', $socket->getAddress()) . PHP_EOL;

function teste()
{

    errorLog("[" . date('d-m-Y H:i:s') . "] ");

    return json_encode(['data' => "true"]);

}

function errorLog($errorMessage)
{
    sleep(1);
    $caminho = __DIR__ . '/Log/simple-log_' . date('d-m-Y') . ".txt";
    $fp = fopen($caminho, "a");
    $errorMessage .= "\n";
    fwrite($fp, $errorMessage);
    fclose($fp);
}

$loop->run();
