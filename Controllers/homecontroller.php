<?php

class HomeController extends Controller
{


    /**
     * @return string
     */
    public function index()
    {

        return $this->response_json(['data' => [
            'resource' => $this->resource,
            'method'   => $this->method,
            'body'     => $this->body,
            'headers'  => $this->headers
        ]]);

    }


}