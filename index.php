<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
header('Access-Control-Allow-Headers: *');

require_once 'config.php';

const BASE_PATH = __DIR__;

try {
    spl_autoload_register(function ($class) {
        if (strpos($class, 'controller') > -1) {

            if (file_exists('Controllers/' . $class . '.php')) {
                require_once 'Controllers/' . $class . '.php';
            }
        } else {
            require_once 'Core/' . $class . '.php';
        }
    });
    $core = new Core;
    $core->handler();
}catch (\Exception $e)
{
    echo $e->getMessage();
}