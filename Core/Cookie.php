<?php

class Cookie {

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $value;

    /**
     * @var int UNIX timestamp
     */
    protected $expires;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var string
     */
    protected $domain;

    /**
     * @var bool
     */
    protected $secure;

    /**
     * @var bool
     */
    protected $httponly;

    /**
     * Slim_Http_Cookie constructor.
     * @param $name
     * @param null $value
     * @param int $expires
     * @param null $path
     * @param null $domain
     * @param bool $secure
     * @param bool $httponly
     */
    public function __construct( $name,
                                 $value    = null,
                                 $expires  = 0,
                                 $path     = null,
                                 $domain   = null,
                                 $secure   = false,
                                 $httponly = false ) {
        $this->setName($name);
        $this->setValue($value);
        $this->setExpires($expires);
        $this->setPath($path);
        $this->setDomain($domain);
        $this->setSecure($secure);
        $this->setHttpOnly($httponly);
    }

    /**
     * Get cookie name
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set cookie name
     * @param   string $name
     * @return  void
     */
    public function setName($name) {
        $this->name = (string)$name;
    }

    /**
     * Get cookie value
     * @return string
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * Set cookie value
     * @param   string $value
     * @return  void
     */
    public function setValue( $value ) {
        $this->value = (string)$value;
    }

    /**
     * Get cookie expiration time
     * @return int UNIX timestamp
     */
    public function getExpires() {
        return $this->expires;
    }

    /**
     * Set cookie expiration time
     * @param   string|int Cookie expiration time
     * @return  void
     */
    public function setExpires( $time ) {
        $this->expires = is_string($time) ? strtotime($time) : (int)$time;
    }

    /**
     * Get cookie path
     * @return string
     */
    public function getPath() {
        return $this->path;
    }

    /**
     * Set cookie path
     * @param   string $path
     * @return  void
     */
    public function setPath( $path ) {
        $this->path = (string)$path;
    }

    /**
     * Get cookie domain
     * @return string
     */
    public function getDomain() {
        return $this->domain;
    }

    /**
     * Set cookie domain
     * @param   string $domain
     * @return  void
     */
    public function setDomain( $domain ) {
        $this->domain = (string)$domain;
    }

    /**
     * Is cookie sent only if SSL/HTTPS is used?
     * @return bool
     */
    public function getSecure() {
        return $this->secure;
    }

    /**
     * Set whether cookie is sent only if SSL/HTTPS is used
     * @param   bool $secure
     * @return  void
     */
    public function setSecure( $secure ) {
        $this->secure = (bool)$secure;
    }

    /**
     * Is cookie sent with HTTP protocol only?
     * @return bool
     */
    public function getHttpOnly() {
        return $this->httponly;
    }

    /**
     * Set whether cookie is sent with HTTP protocol only
     * @param   bool $httponly
     * @return  void
     */
    public function setHttpOnly( $httponly ) {
        $this->httponly = (bool)$httponly;
    }

}