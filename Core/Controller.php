<?php

/**
 * Class Controller
 */
class Controller extends Request{

    /**
     * @return mixed
     * @throws Exception
     */
//	public function getRequest()
//    {
//       
//        if (strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0) {
//            $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
//
//            if (strcasecmp($contentType, 'application/json;charset=UTF-8') == 0) {
//                $_POST = json_decode(trim(file_get_contents("php://input")));
//            }
//            return $_POST;
//        } else {
//            return $_GET;
//        }
//
//    }

    /**
     * @param $data
     * @return string
     */
    public function response_json($data)
    {
        @header('Content-Type: application/json');
        echo json_encode($data);
    }

    /**
     * @param $errorMessage
     */
    public function Log($errorMessage)
    {
        $caminho      = BASE_PATH . '/Log/simple-log_'.date('d-m-Y').".txt";
        $fp           = fopen($caminho, "a");
        $errorMessage.= "\n";
        fwrite($fp, $errorMessage);
        fclose($fp);
    }

}

 ?>