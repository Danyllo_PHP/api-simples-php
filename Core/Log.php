<?php

/**
 * Class Log
 */
class Log
{
    
    /**
     * @param $errorMessage
     */
    public function errorLog($errorMessage)
    {
        $caminho      = BASE_PATH . '/Log/simple-log_'.date('d-m-Y').".txt";
        $fp           = fopen($caminho, "a");
        $errorMessage.= "\n";
        fwrite($fp, $errorMessage);
        fclose($fp);
    }


    public function alertLog($errorMessage)
    {
        sleep(1);
        $caminho      = BASE_PATH . '/Log/React-log_'.date('d-m-Y').".txt";
        $fp           = fopen($caminho, "a");
        $errorMessage.= "\n";
        fwrite($fp, $errorMessage);
        fclose($fp);
    }

}